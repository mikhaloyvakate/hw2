import User from '../models/User.js'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

export const getMe = async (req, res) => {
    try {
        
        const user = await User.findById(req.userId)
        
        if (!user) {
            return res.status(400).json({
                message: 'This user does not exist.',
            })
        }

        const token = jwt.sign(
            {
                id: user._id,
            },
            process.env.JWT_SECRET,
            { expiresIn: '30d' },
        )


        res.status(200).json({
            user
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}

export const deleteMe = async (req, res) => {
    try {
        
        const user = await User.findById(req.userId).remove();
        
        res.status(200).json({
         message:  "Success"
        })
    } catch (error) {
        res.status(500).json({ message: 'Error' })
    }
}

export const updateMe = async (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body

        let user = await User.findById(req.userId);
        

        const salt = bcrypt.genSaltSync(10)
        const newhash = bcrypt.hashSync(newPassword, salt)
        
            user.update({
                password: newhash
            })

            res.status(200).json({
                message: "Success"
            })
        
    } catch (error) {
        res.status(500).json({ message: 'Error' })
    }
}