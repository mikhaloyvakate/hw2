import User from '../models/User.js'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

export const register = async (req, res) => {
    try {
        const { username, password } = req.body

        const isUsed = await User.findOne({ username })

        if (isUsed) {
            return res.status(404).json({
                message: 'User name is alredy taken',
            })
        }

        const salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)

        const newUser = new User({
            username,
            password: hash,
        })

        const token = jwt.sign(
            {
                id: newUser._id,
            },
            process.env.JWT_SECRET,
            { expiresIn: '30d' },
        )

        await newUser.save()

        res.status(200).json({
            message: 'Success',
        })
    } catch (error) {
        res.status(500).json({ message: 'Error' })
    }
}

export const login = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username })
        if (!user) {
            return res.status(404).json({
                massage: `User ${user} is not found`            })
        }
        const isPasswordCorrect = await bcrypt.compare(password, user.password);
        
        if (!isPasswordCorrect) {
            res.status(404).json({
                message: 'Password not correct'
            })
        }

        const token = jwt.sign({
            id: user.id,
        },
        process.env.JWT_SECRET,
        {expiresIn:'30d'}
        )

        res.status(200).json({
            
            message: 'Success',
            jwt_token: token
        })


    } catch (err) {
        res.status(500).json({
            message:'Error'
        })
    }
}

