import mongoose from 'mongoose'

const NotesSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
            required: true
        },
        completed: {
            type: Boolean,
            required: true,
        },
        text: {
            type: String,
            required: true
        }
    },
    { timestamps: true },
)

export default mongoose.model('Notes', NotesSchema)