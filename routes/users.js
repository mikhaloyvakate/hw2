import { Router } from 'express'
import { getMe, deleteMe, updateMe } from '../controllers/users.js'
import { checkAuth } from '../utils/checkAuth.js'
const router = new Router()


router.get('/me',checkAuth, getMe)

router.delete('/me',checkAuth, deleteMe)

router.patch('/me',checkAuth, updateMe)



export default router